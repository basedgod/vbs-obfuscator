﻿using System;
using System.Windows;
using System.Threading;

namespace vbs_obfuscator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (vbsTextBox.Text.Equals(String.Empty) || vbsTextBox.Text.Equals("VBS"))
            {
                MessageBox.Show("Enter Text.");
                return;
            }
            if (vbsTextBox.Text.Contains("§"))
            {
                MessageBox.Show("This text was already obfuscated.");
                return;
            }
            var oEncoder = new Obfuscator(vbsTextBox.Text);
            var oEncoderThread = new Thread(oEncoder.Encode);
            oEncoderThread.Start();
            while (oEncoderThread.IsAlive)
            {
                
            }

            if (radioRandomize.IsChecked == true)
            {
                var randomVariableCharBuffer = new char[8];
                for (var i = 0; i < 8; i++)
                {
                    var rngRandom = new Random((int) DateTime.Now.Ticks + i);

                    randomVariableCharBuffer[i] =
                        Convert.ToChar(Convert.ToInt32(Math.Floor(26*rngRandom.NextDouble() + 65)));
                }
                var randomVariable = new String(randomVariableCharBuffer);
                var obfuscatedText = randomVariable + " = (";
                vbsTextBox.Text = obfuscatedText + oEncoder.GetEncoded() + ")";
                vbsTextBox.Text += "\n\n\n\n\n";
                vbsTextBox.Text += randomVariable + "= (split)("+ randomVariable+ ",\"§\")";
                vbsTextBox.Text += "\n";
                vbsTextBox.Text += "for i = (0) to ubound(" + randomVariable + ") - 1\n";
                vbsTextBox.Text += "Man = Man & chr(" + randomVariable + "(i)+17)\n";
                vbsTextBox.Text += "next\n";
                vbsTextBox.Text += "execute (Man)";
            }
            else
            {
              
                vbsTextBox.Text = "CODE = (" + oEncoder.GetEncoded() + ")";
                vbsTextBox.Text += "\n\n\n\n\n";
                vbsTextBox.Text += "CODE = (split)(CODE,\"§\")";
                vbsTextBox.Text += "\n";
                vbsTextBox.Text += "for i = (0) to ubound(CODE) - 1\n";
                vbsTextBox.Text += "Man = Man & chr(CODE(i)+17)\n";
                vbsTextBox.Text += "next\n";
                vbsTextBox.Text += "execute (Man)";
             }

            



        }

        private void TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            if (vbsTextBox.Text.Equals("VBS"))
                vbsTextBox.Text = "";
        }

        private void vbsTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (vbsTextBox.Text.Equals(""))
                vbsTextBox.Text = "VBS";
        }
    }

    public class Obfuscator
    {
        private string TextToEncode { get; set; }
        private string EncodedText { get; set; }
        public Obfuscator(string textToEncode)
        {
            TextToEncode = textToEncode;
        }

        public void Encode()
        {
            foreach (var c in TextToEncode)
            {
                var charVal = c + 17;
                EncodedText += charVal + "§";
            }
        }

        internal string GetEncoded()
        {
            return EncodedText;
        }
    }
}
